all : compile

compile : dht11.o httpHomebridge.o
	gcc temp.c -Wall -o temp -lwiringPiDev -lpthread -lwiringPi -largtable2 -lcurl dht11.o httpHomebridge.o
run : compile
	./temp
debug : dht11.o
	gcc temp.c -o temp.debug -g -lwiringPiDev -lpthread -lwiringPi -largtable2 -lcurl dht11.o httpHomebridge.o
	gdb temp.debug
clean :
	rm -f temp.o temp temp.debug dht11.o httpHomebridge.o
dht11.o :
	gcc -c -Wall dht11.c 
httpHomebridge.o:
	gcc -c -Wall httpHomebridge.c
