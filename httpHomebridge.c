#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>

int sendHttpUpdate (char* host, int port, char* sensor, char* value, int verbose){
	char url[255];
	int logicalValue = strcmp("true", value) || strcmp("false", value);
	char* arg = logicalValue ? "state" :"value";
	sprintf(url, "http://%s:%d/?accessoryId=%s&%s=%s", host, port, sensor, arg, value); 
	if (verbose) printf("updating sensor: %s <= %s [%s]\n", sensor, value, url);
	
	CURL *curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(curl, CURLOPT_URL, url);
	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "cache-control: no-cache");
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	CURLcode ret = curl_easy_perform(curl);
	
	/* Check for errors */ 
    if(ret != CURLE_OK) fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(ret));
    /* always cleanup */ 
    curl_easy_cleanup(curl);
	return 200;
}
