#include "dht11.h"
#include "httpHomebridge.h"
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <argtable2.h>

#define DHTPIN 7 //GPIO 4
#define MIN_CYCLE_MILIS 1200
#define DEFAULT_RETRIES 3
#define DEFAULT_FREEZING 19
#define DEFAULT_HB_HOST "127.0.0.1"
#define DEFAULT_HB_PORT 51828
#define DEFAULT_HB_TEMP_SENSOR "sensor5"
#define DEFAULT_HB_HUMIDITY_SENSOR "sensor6"
#define DEFAULT_HB_FREEZE_SENSOR "sensor7"


int mymain(
    int retries, 
    int coldLimitCelsius, 
    int notify, 
    const char* host, 
    int port, 
    const char* snTemp,
    const char* snHum,
    const char* snFrezz,
    int verbose)
{
    if(verbose){
      printf( " - retries: %d\n", retries);
      printf( " - cold: %d\n", coldLimitCelsius);
      printf( " - notify: %d\n", notify);
      printf( " - host: %s\n", host);
      printf( " - port: %d\n", port);
      printf( " - snTemp: %s\n", snTemp);
      printf( " - snHum: %s\n", snHum);
      printf( " - snFrezz: %s\n", snFrezz);
    }
    
    int reads[5] = {0,0,0,0,0};
    int ok = FALSE;
    int retriesLeft = retries;
    while (!ok && retriesLeft-- ){
        ok = read_dht11_dat(reads, DHTPIN, verbose);
        if(!ok) { 
            delay (MIN_CYCLE_MILIS);
        }
    }
    int result = reads[2] > coldLimitCelsius ? 0:1; 
    if(ok) {
        printf( "Humidity = %d.%d %% Temperature = %d.%d *C \n", reads[0], reads[1], reads[2], reads[3]);
        if(notify){
            char value[20];
            sprintf(value, "%d.%d", reads[2], reads[3]);
            sendHttpUpdate (host, port, snTemp, value, verbose);
            sprintf(value, "%d.%d", reads[0], reads[1]);
            sendHttpUpdate (host, port, snHum, value, verbose);
            sendHttpUpdate (host, port, snFrezz, result?"true":"false" , verbose);
        }
    }
    if (verbose) printf("returning %d\n",result);
	return(result);
}

int main( int argc, char**argv ){

    /* Define the allowable command line options, collecting them in argtable[] */
    struct arg_int *frez  = arg_int0("f", "freeze",NULL,   "limit temperature for consider 'freezing' (celsius)");
    struct arg_int *repeat= arg_int0("r","retries",NULL,              "define scalar value k (default is 3)");
    struct arg_lit *n     = arg_lit0("n","notify",      "notify temperature and humidity homekit via ws");
    struct arg_str *host  = arg_str0(NULL,"host","<HOSTNAME>",         "ip or addres of the homebridge server (localhost)");
    struct arg_int *port  = arg_int0("p","port" ,"<PORT>",             "port of homebridge-httpWebHooks plugin (51828)");
    struct arg_str *stn   = arg_str0(NULL,"temp","<sensor_name>",         "name of the configured sensor (sensor5)");
    struct arg_str *shn   = arg_str0(NULL,"humidity","<sensor_name>",         "name of the configured sensor (sensor6)");
    struct arg_str *shf   = arg_str0(NULL,"cold","<sensor_name>",         "name of the configured sensor (sensor7)");
    struct arg_lit *verb  = arg_lit0("v","verbose",     "be more verbose");
    struct arg_lit *help  = arg_lit0(NULL,"help",       "print this help and exit");
    struct arg_lit *vers  = arg_lit0(NULL,"version",    "print version information and exit");
    struct arg_end *end   = arg_end(20);
    void* argtable[] = {frez,repeat,n,host,port,stn,shn,shf,verb,help,vers,end};
    const char* progname = "temp";
    int exitcode=0;
    int nerrors;

    /* verify the argtable[] entries were allocated sucessfully */
    if (arg_nullcheck(argtable) != 0)
        {
        /* NULL entries were detected, some allocations must have failed */
        printf("%s: insufficient memory\n",progname);
        exitcode=1;
        goto exit;
        }

    /* default values */
    repeat->ival[0] = DEFAULT_RETRIES;
    frez->ival[0] = DEFAULT_FREEZING;
    host->sval[0] = DEFAULT_HB_HOST;
    port->ival[0] = DEFAULT_HB_PORT;
    stn->sval[0] = DEFAULT_HB_TEMP_SENSOR;
    shn->sval[0] = DEFAULT_HB_HUMIDITY_SENSOR;
    shf->sval[0] = DEFAULT_HB_FREEZE_SENSOR;
    
    /* Parse the command line as defined by argtable[] */
    nerrors = arg_parse(argc,argv,argtable);

    /* special case: '--help' takes precedence over error reporting */
    if (help->count > 0)
        {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout,argtable,"\n");
        printf("Echo the STRINGs to standard output.\n\n");
        arg_print_glossary(stdout,argtable,"  %-10s %s\n");
        exitcode=0;
        goto exit;
        }

    /* special case: '--version' takes precedence error reporting */
    if (vers->count > 0)
        {
        printf("Raspberry Pi wiringPi DHT11 Temperature test program\nV0.2\n" );
        exitcode=0;
        goto exit;
        }

    /* If the parser returned any errors then display them and exit */
    if (nerrors > 0)
        {
        /* Display the error details contained in the arg_end struct.*/
        arg_print_errors(stdout,end,progname);
        printf("Try '%s --help' for more information.\n",progname);
        exitcode=1;
        goto exit;
        }

    /* Command line parsing is complete, do the main processing */
    //mymain(4);
    mymain(repeat->ival[0], frez->ival[0], n->count, host->sval[0], port->ival[0], stn->sval[0], shn->sval[0], shf->sval[0], verb->count);

    exit:
    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

    return exitcode;
  
}
