#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define MAXTIMINGS 85 //total de flancos durante la transmisión
#define MAX_ZERO_TIME 40 //tiempo maximo de un pulso 0 (en el ejemplo 16, pero estadisticamente 35, tiempo maximo de 1: 70)

int read_dht11_dat(int dht11_dat[5], int datapin, int verbose){
    uint8_t laststate = HIGH;
    uint8_t counter = 0;
    uint8_t j = 0, i;

    if ( wiringPiSetup() == -1 ){
		exit( 1 );
	}
    
    //reset the result array
    for(int byte=0; byte <5 ; byte++){
        dht11_dat[byte]=0;
    }

    //pull pin down for 18 miliseconds
    pinMode (datapin, OUTPUT);
    digitalWrite(datapin, LOW);
    delay(18);
    //pull pin up for 40 microsecs
    digitalWrite(datapin, HIGH);
    delayMicroseconds(40);
    //prepare to read the pin
    pinMode (datapin, INPUT);
    //detect change and read data
    for(i=0; i < MAXTIMINGS ; i++ ){
        counter = 0;
        while (digitalRead (datapin) == laststate){
            counter ++;
            delayMicroseconds(1);
            if(counter == 255 ) break;
        }
        laststate = digitalRead(datapin);
        //printf("|%d-%d:%d",i,counter,laststate);
        if (counter == 255) break;

        //ignore first 3 transitions
        if ( i >= 4 && (i%2 == 0)){
            //printf(" =>%d",j/8);
            dht11_dat[j/8] <<= 1;
            if (counter > MAX_ZERO_TIME){
                dht11_dat[j/8] |= 1;
            }
            j++;
        }
    }

    //check we read 40 bits + verify chcksum
    int okLength = j >= 40;
    if(verbose) printf("data lengt:%d ,",j);
    int okCheck = okLength && dht11_dat[4] == ((dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3]) & 0xFF );
    printf("data: [ %x , %x , %x , %x ] (%x) ",dht11_dat[0], dht11_dat[1], dht11_dat[2], dht11_dat[3], dht11_dat[4]);
    if(verbose){
        if (okCheck){
            printf( "Humidity = %d.%d %% Temperature = %d.%d *C \n",
                dht11_dat[0], dht11_dat[1], dht11_dat[2], dht11_dat[3]);
        } else {
            printf ("data not good, skip\n");
        }
    }
    return okCheck;
}
